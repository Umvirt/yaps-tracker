#Module: Deadline
RewriteRule ^tracker(/?)$ %sitepath%/index.php?ns=tracker
RewriteRule ^tracker/([0-9.]+)(/?)$ %sitepath%/index.php?ns=tracker&id=$1
RewriteRule ^tracker/products(/?)$ %sitepath%/index.php?ns=tracker&controller=products
RewriteRule ^tracker/products/([0-9.]+)(/?)$ %sitepath%/index.php?ns=tracker&controller=products&id=$1

RewriteRule ^tracker/questions(/?)$ %sitepath%/index.php?ns=tracker&controller=questions
RewriteRule ^tracker/questions/([0-9.]+)(/?)$ %sitepath%/index.php?ns=tracker&controller=questions&id=$1&action=editfrm
RewriteRule ^tracker/question/([0-9.]+)(/?)$ %sitepath%/index.php?ns=tracker&id=$1&action=question

RewriteRule ^tracker/answers(/?)$ %sitepath%/index.php?ns=tracker&controller=answers
RewriteRule ^tracker/answers/([0-9.]+)(/?)$ %sitepath%/index.php?ns=tracker&controller=answers&id=$1&action=editfrm


