<?php
class TrackerProduct{
var $id;
var $name;
var $parent;
var $owner;
var $description;
}

class TrackerProducts {
function __construct($db){
$this->db=$db;

$this->items_sql="select id, owner, name, parent, `description` from tracker_products";

}

//----
/**
* Get product information array by id
*/
function getProduct($id){
        global $db;

        $sql=$this->items_sql." where id=".$id;
        $db->execute($sql);

        $x=$db->dataset;

        if(count($x)){
                foreach($x as $k=>$v){
                        return array('id'=>$id,'name'=>$v['name'],'parent'=>$v['parent'], 'description'=>$v['description']);
                }
        }
}

/**
* Get path for product with specific id
*/
function getPath($id){
        $path=array();
        $this->getLevel($id,$path);
        return $path;
}

/**
* Get product path level data.
* Recursion!
*/
function getLevel($id,&$path){
        $x=$this->getProduct($id);
        //$path[]=$x;
        if($x['parent']){
                $this->getLevel($x['parent'],$path);
        }
if($x){
        $path[]=$x;
}
}


//----



function add($obj){
$parent="NULL";
if($obj->parent){
$parent=$obj->parent;
}

$sql[]="insert into tracker_products (owner, name, parent, description) values ($obj->owner,\"$obj->name\",$parent,\"$obj->description\")";
$sql[]="select @@identity";
//echo $sql;

$this->db->execute($sql);

foreach($this->db->dataset as $v){
return $v['@@identity'];

//var_dump($sql,$db->errors);

}


}



function delete($ids){

$ids_=join($ids,",");
$sql[]="delete from tracker_products where id in ($ids_)";
$sql[]="select from tracker_products where id in ($ids_)";

$this->db->execute($sql);
//echo $sql;
//exit;
return (count($this->db->dataset)==0);

}





function mapper($dataset){
$x=array();
foreach($dataset as $v){
$obj=new DeadlineDate;
$obj->id=$v['id'];
$obj->owner=$v['owner'];
$obj->parent=$v['parent'];
$obj->name=$v['name'];
$obj->description=$v['description'];

$x[]=$obj;
}

return $x;

}


function items($parent=''){
if($parent){
$sql=$this->items_sql." where parent=$parent";
}else{
$sql=$this->items_sql." where parent is NULL";
}

//var_dump($sql, $this->db->errors);

//$sql.=" where r.`release`=\"$release\"";
//$sql.=" order by p.id";
$this->db->execute($sql);
return $this->mapper($this->db->dataset);
}

function actualitems(){
$sql=$this->items_sql." where date>=current_date() order by date";
//$sql.=" where r.`release`=\"$release\"";
//$sql.=" order by p.id";
$this->db->execute($sql);
return $this->mapper($this->db->dataset);
}



function item($id){
$sql=$this->items_sql." where id=$id";
$this->db->execute($sql);
return $this->mapper($this->db->dataset);
}

function dates(){
$sql=$this->datecount_sql." group by date";
//$sql.=" where r.`release`=\"$release\"";
//$sql.=" order by p.id";
$this->db->execute($sql);
return $this->db->dataset;
}


function save($obj){
$sql="update tracker_products set name=\"$obj->name\", description=\"$obj->description\" where id=$obj->id";
echo $sql;

$this->db->execute($sql);

//exit;

return true;

}




}
